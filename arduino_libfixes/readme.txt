copy these files into their appropriate location, overwriting the default library files.
PubSubclient.h : needs a bigger static mqtt packet buffer
OLEDDisplayUi.cpp : the update() function didn't work correctly, when the function is not called fast enough.
The files core_esp8266_sigma_delta.c and sigma_delta.h have to end up in the esp8266 platform directory. On Windows the path is something like : 
C:\Users\xxx\AppData\Local\Arduino15\packages\esp8266\hardware\esp8266\2.4.0\cores\esp8266