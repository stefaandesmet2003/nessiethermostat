#ifndef _AWSIOT_H_
#define _AWSIOT_H_

void awsiot_setup();
void awsiot_loop();
void awsiot_publishUpdate(); // triggers a mqtt publish

#endif // _AWSIOT_H_
