#include "FS.h"
#include <ESP8266WiFi.h>

// override library default: doesn't work --> we have to modify in PubSubClient.h !!
//#define MQTT_MAX_PACKET_SIZE 1024

#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "heatcontrol.h" // get/set of heating parameters

static void mqttConnect();
static void subscribeCallback(char* topic, byte* payload, unsigned int length);

// my AWS IOT characteristics
extern const char *aws_endpoint; // = "aaaaaaaaaaaaaa.iot.eu-west-1.amazonaws.com"; //AWS IOT MQTT broker
extern const char *aws_topic_update; // = "$aws/things/yourthingname/shadow/update";
extern const char *aws_topic_update_accepted; // = "$aws/things/yourthingname/shadow/update/accepted";
// const char* aws_topic_update_delta = "$aws/things/yourthingname/shadow/update/delta";
// const char* aws_topic_update_rejected = "$aws/things/yourthingname/shadow/update/rejected";
//const char* aws_topic_update_all = "$aws/things/yourthingname/shadow/update/#";

bool topicUpdate = false;
// ugly : order corresponds with the thermostatMode_t type in heatcontrol.h (CLOCK-DAY-NIGHT-FROST)
const char *thermostatModeStrings[] = {"AUTO","HEAT","ECO","OFF"};
StaticJsonBuffer<512> jsonBuffer; // parsing incoming json docs;
WiFiClientSecure espClient;
PubSubClient client(aws_endpoint, 8883, subscribeCallback, espClient); //set  MQTT port number to 8883 as per //standard

#define MQTT_AUTORECONNECT_DELAY 5000
uint32_t mqttLastConnectionAttemptMillis = -MQTT_AUTORECONNECT_DELAY;

#define MQTT_PUBLISH_DELAY 5000 // publish max once every 5 seconds - prevent that every UI action on the device is published
uint32_t mqttPublishMillis; 

// triggers a mqtt publish
void awsiot_publishUpdate() {
  topicUpdate = true;
}

void awsiot_setup() {

  Serial.printf("MQTT_MAX_PACKET_SIZE = %d", MQTT_MAX_PACKET_SIZE);
  
  if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
    return;
  }
  // SPIFFS.begin is called in main setup()

  // Load certificate file
  File cert = SPIFFS.open("/cert.der", "r"); // certificate file in .der format
  if (!cert) {
    Serial.println("Failed to open cert file");
  }
  else
    Serial.println("Success to open cert file");

  if (espClient.loadCertificate(cert))
    Serial.println("cert loaded");
  else
    Serial.println("cert not loaded");

  // Load private key file
  File private_key = SPIFFS.open("/key.der", "r"); // private key file in .der format
  if (!private_key) {
    Serial.println("Failed to open private cert file");
  }
  else
    Serial.println("Success to open private cert file");

  if (espClient.loadPrivateKey(private_key))
    Serial.println("private key loaded");
  else
    Serial.println("private key not loaded");
  
  // we'll never set up the mqtt connection without these certificates, so we might as well quit here and never serve alexa_loop ..
  // can we close the *.der files now? looks like yes, but no heap gain ..
  /*
  private_key.close();
  cert.close();
  */
  /*
    // Load CA file
    File ca = SPIFFS.open("/ca.der", "r"); //replace ca with your uploaded file name
    if (!ca) {
      Serial.println("Failed to open ca ");
    }
    else
    Serial.println("Success to open ca");

    if(espClient.loadCACert(ca))
    Serial.println("ca loaded");
    else
    Serial.println("ca failed");
  */
  mqttConnect();
  Serial.print("Heap: "); Serial.println(ESP.getFreeHeap());
} // awsiot_setup

void awsiot_loop() {

  if ((!client.connected()) && ((millis() - mqttLastConnectionAttemptMillis ) > MQTT_AUTORECONNECT_DELAY)) {
    mqttConnect();
    mqttLastConnectionAttemptMillis = millis();
  }
  else {
    // mqtt loop
    client.loop();
    
    // don't publish more than once every MQTT_PUBLISH_DELAY ms
    if ((topicUpdate) && ((millis() - mqttPublishMillis) > MQTT_PUBLISH_DELAY)) {
      mqttPublishMillis = millis();
      
      /* it would be nice to be able to use the static jsonBuffer here, but we need an extra buffer anyway for the serialized output 
         (or can we stream into the mqtt internal buffer?)
         so we might as well use a char[] buffer straight away.. */
      // client.publish copies the payload to an internal buffer of size MQTT_MAX_PACKET_SIZE!

      char *mqttTopicBuffer = (char*)malloc(300);
      if (mqttTopicBuffer) {
        float targetSetpoint = hc_GetTargetSetpoint();
        float indoorTemp = hc_GetIndoorTemp();
        float indoorHum = hc_GetIndoorHum();
        int heatPercentage = hc_GetHeatPercentage();
        const char *thermostatModeString = thermostatModeStrings[hc_GetThermostatMode()];
        snprintf(mqttTopicBuffer, 300, "{\"state\": {"
                                  "\"reported\": {"
                                  "\"targetSetpoint\" : %f,"
                                  "\"indoorTemperature\" : %f,"
                                  "\"indoorHumidity\" : %f,"
                                  "\"heatPercentage\" : %d,"
                                  "\"thermostatMode\" : \"%s\"}}}" ,targetSetpoint,indoorTemp,indoorHum,heatPercentage,thermostatModeString);
        client.publish(aws_topic_update,mqttTopicBuffer);
        delete(mqttTopicBuffer);
      }
      else {
        Serial.println("malloc(300) failed; cannot publish to mqtt!");
      }
      topicUpdate = false; // reset the flag
      Serial.printf("Heap: %d\n",ESP.getFreeHeap());
    }
  }

} // awsiot_loop

// callback called every time a message is received on aws_topic_update_accepted[]
static void subscribeCallback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  
  if (strcmp(topic,aws_topic_update_accepted) == 0) {
    // parse the payload
    jsonBuffer.clear(); // start new parse operation
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (!root.success()) {
      Serial.printf("json parsing error on topic %s",topic);
      // parsing failed
      return;
    }
    
    // some useful explanation here : https://arduinojson.org/api/jsonobject/containskey/
    if (root["state"]["desired"]) {
      
      // app sends a request for a parameter change
      if (root["state"]["desired"]["heatPercentage"]) {
        // heatPercentage key exists
        hc_SetHeatPercentage(root["state"]["desired"]["heatPercentage"]);
      }
      if (root["state"]["desired"]["targetSetpoint"]) {
        // targetSetpoint key exists
        hc_SetTargetSetpoint(root["state"]["desired"]["targetSetpoint"]);
      }
      
      const char *thermostatModeString = root["state"]["desired"]["thermostatMode"];
      if (thermostatModeString) {
        // thermostatMode key exists
        for (int mode=0;mode<4;mode++) {
          if (0== strcmp(thermostatModeStrings[mode],thermostatModeString)) {
            hc_SetThermostatMode(mode);
            Serial.printf("matched thermostatMode %s to %d\n",thermostatModeString,mode);
            break;
          }
        }
      }  
         
      // update is done, now publish an updated of the "reported" parameters
      // main loop will publish an update; don't care for now if nothing has changed
      topicUpdate = true; 
    }
  }
  else {
    // we don't handle any other topics yet
  }
} // subscribeCallback

// setup/restore mqtt connection to AWS IOT, and setup subscription(s)
static void mqttConnect() {
  if (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESPthing")) {
      Serial.println("connected");
      if (!client.subscribe(aws_topic_update_accepted)) {
        Serial.printf("subscription to %s failed!",aws_topic_update_accepted);
      }
      Serial.print(client.state());
    } 
    else {
      Serial.print("mqtt connection failed, error =");
      Serial.print(client.state());
    }
  }
} // mqttConnect

