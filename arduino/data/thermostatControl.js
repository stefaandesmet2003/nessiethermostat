
var targetSetpointSlider = document.getElementById("targetSetpointRange");
var targetSetpointTxt = document.getElementById("targetSetpoint");
targetSetpointTxt.innerHTML = targetSetpointSlider.value;
var heatPercentageSlider = document.getElementById("heatPercentageRange");
var heatPercentageTxt = document.getElementById("heatPercentage");
heatPercentageTxt.innerHTML = heatPercentageSlider.value;
var currentTime = document.getElementById("currentTime");
var indoorTemperature = document.getElementById("indoorTemperature");
var indoorHumidity = document.getElementById("indoorHumidity");
var wifiSSID = document.getElementById("wifiSSID");
var wifiRSSI = document.getElementById("wifiRSSI");

targetSetpointSlider.oninput = function() {
  targetSetpointTxt.innerHTML = this.value;
}
targetSetpointSlider.onchange = function() {
  // onchange fires with every keyboard stroke on the element, and when mouse clicked (mouseup)
  // we don't want this to fire on every input change, in particular during sliding
  cmdSetTargetSetpoint();
}
heatPercentageSlider.oninput = function() {
  heatPercentageTxt.innerHTML = this.value;
}
heatPercentageSlider.onchange = function() {
  cmdSetHeatPercentage();
}

function cmdGetDeviceStatus() {
    var xmlhttp = new XMLHttpRequest();
    var req = "/nest"; // TODO : not yet implemented on hw
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var deviceStatus = JSON.parse(this.response); // of responseText ??
          updatePageContents(deviceStatus);
        }
    };
    xmlhttp.open("GET", req, true);
    xmlhttp.send();
}
function cmdSetTargetSetpoint() {
    var xmlhttp = new XMLHttpRequest();
    var req = "/nest?temp="+targetSetpointSlider.value;
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var deviceStatus = JSON.parse(this.response); // of responseText ??
          updatePageContents(deviceStatus);
        }
    };
    xmlhttp.open("GET", req, true);
    xmlhttp.send();
} // cmdSetTargetSetpoint

function cmdSetHeatPercentage() {
    var xmlhttp = new XMLHttpRequest();
    var req = "/nest?heat="+heatPercentageSlider.value;
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var deviceStatus = JSON.parse(this.response); // of responseText ??
          updatePageContents(deviceStatus);
        }
    };
    xmlhttp.open("GET", req, true);
    xmlhttp.send();
} // cmdSetHeatPercentage

function updatePageContents(deviceStatus)
{
  targetSetpointSlider.value = deviceStatus.targetSetpoint;
  targetSetpointTxt.innerHTML = targetSetpointSlider.value;
  heatPercentageSlider.value = deviceStatus.heatPercentage;
  heatPercentageTxt.innerHTML = heatPercentageSlider.value;
  var deviceTime = new Date(1000*deviceStatus.time); // device works with seconds since 1970, not ms
  currentTime.innerHTML = deviceTime.toString();
  indoorTemperature.innerHTML = deviceStatus.indoorTemperature;
  indoorHumidity.innerHTML = deviceStatus.indoorHumidity;
  indoorTemperature.innerHTML = deviceStatus.indoorTemperature;
  wifiSSID.innerHTML = deviceStatus.wifiSSID;
  wifiRSSI.innerHTML = deviceStatus.wifiRSSI;
} // updatePageContents

// update page with live device status
cmdGetDeviceStatus();
