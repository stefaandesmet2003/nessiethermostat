#include "arduino.h"
#include "heatcontrol.h"
#include "sigma_delta.h"
#include "myntp.h"
#include "TimeLib.h" // for the functions day(), month(), year() etc
#include <ESP8266WebServer.h>
#include <FS.h> // file functions, to use with SPIFFS object
#include "awsiot.h" // for triggering mqtt updates when heating parameters change.

// for the SI7021
#include <Wire.h>

#define si7021Addr                    0x40 // i2c address of the temperature/humidity sensor

#define SD_TARGET_MIN                 75  // 0.78V measured output 
#define SD_TARGET_MAX                 215 // 1.014V measured output

#define HEAT_RANGE                    100 // heatPercentage : 0-100%
#define HEAT_DEFAULT_FROST            0  // frost protection mode in gas heater
#define HEAT_DEFAULT_DAY              75 // %, 
#define HEAT_DEFAULT_NIGHT            15 // %, 
#define HEAT_DEFAULT_FASTHEAT         95 // %, was 100

#define TEMP_SETPOINT_MIN             10.0
#define TEMP_SETPOINT_MAX             26.0
#define TEMP_SETPOINT_DEFAULT_DAY     23.5
#define TEMP_SETPOINT_DEFAULT_NIGHT   18.0
#define TEMP_SETPOINT_MAX_OFFSET      0.5 // max deviation from setpoint temperature

#define SDCONTROL_PIN                 2   // D4 = IO2
#define SENSOR_READ_INTERVAL          60000 // 1 minute
#define OUTDOOR_LOG_INTERVAL          600000 // 10 minutes - to match with the reading frequency by the weather-module

extern ESP8266WebServer server; // the app's webserver instance

// read the sensor data every SENSOR_READ_INTERVAL ms
uint32_t lastSensorMillis = -SENSOR_READ_INTERVAL;
uint32_t lastOutdoorDataMillis = -OUTDOOR_LOG_INTERVAL; // wunderground updates every 10 minutes
float indoorHum=0.0, indoorTemp=0.0, outdoorTemp=0.0; // actual sensor data being used

#define NBR_INDOORTEMP_POINTS 15
float indoorTemps[NBR_INDOORTEMP_POINTS]; // avg over 15 SENSOR_READ_INTERVAL is used for the UI
uint8_t indoorTempsIdx = 0;

float hc_TargetSetpoint = TEMP_SETPOINT_DEFAULT_DAY;
uint8_t hc_ThermostatMode = MODE_CLOCK;
// learning thermostat
// todo : adapt schedule to learning
uint8_t hc_HeatPercentageDay = HEAT_DEFAULT_DAY;
uint8_t hc_HeatPercentageNight = HEAT_DEFAULT_NIGHT;

static void readSi7021(float *rh, float *temp);
static void run7021();

float hc_GetIndoorTemp() { 
  return indoorTemp;
}
float hc_GetIndoorHum() { 
  return indoorHum;
}
void hc_SetTargetSetpoint (float targetSetpoint) {
  if ((targetSetpoint > TEMP_SETPOINT_MIN) && (targetSetpoint <= TEMP_SETPOINT_MAX)) {
    hc_TargetSetpoint = targetSetpoint;
    awsiot_publishUpdate();
  }
  // else don't touch the actual target
}

float hc_GetTargetSetpoint (void) {
  return hc_TargetSetpoint;
}

// average indoor temperature last 15'
float hc_GetAvgIndoorTemp() {
  float avgIndoorTemp = 0;
  for (int i=0; i<NBR_INDOORTEMP_POINTS;i++) {
    avgIndoorTemp += indoorTemps[i];
  }
  return (avgIndoorTemp / NBR_INDOORTEMP_POINTS);
}

void hc_setup()
{
  pinMode(SDCONTROL_PIN,OUTPUT); // gebeurt eigenlijk ook door attachPin hieronder
  sigmaDeltaEnable();
  sigmaDeltaAttachPin(SDCONTROL_PIN);
  sigmaDeltaSetPrescaler(10);
  
  hc_SetThermostatMode(MODE_CLOCK);
  hc_SetHeatPercentage(HEAT_DEFAULT_DAY,false);

  // i2c needed for si7021
  Wire.begin();

} // hc_setup

void hc_loop()
{
  uint8_t curHeatPercentage = hc_GetHeatPercentage();
  uint8_t newHeatPercentage;
  uint32_t secsInDay = ntp_GetDateTime() % 86400;
  uint32_t secs6AM = 6*3600;
  uint32_t secs8AM = 8*3600;
  uint32_t secs10PM = 22*3600;
  
  switch (hc_ThermostatMode) {
    case MODE_NIGHT :
      newHeatPercentage = hc_HeatPercentageNight;
      // switch to clock mode in the morning
      if (secsInDay == secs6AM)
        hc_ThermostatMode = MODE_CLOCK;
      break;
    case MODE_CLOCK :
      // FASTHEAT tussen 6 en 8u 's ochtends
      if ((secsInDay >= secs6AM) && (secsInDay < secs8AM))
      { 
        newHeatPercentage = HEAT_DEFAULT_FASTHEAT;
      }
      else if ((secsInDay >= secs8AM) && (secsInDay < secs10PM))
      {
        newHeatPercentage = hc_HeatPercentageDay;
      }
      else if ((secsInDay >= secs10PM) || (secsInDay < secs6AM))
      {
        newHeatPercentage = hc_HeatPercentageNight;
      }
      break;
    case MODE_DAY : 
      newHeatPercentage = hc_HeatPercentageDay;
      break;
    case MODE_FROST : 
      newHeatPercentage = HEAT_DEFAULT_FROST;
      break;
  }
  // simple control mechanism for now
  // avoid overheating when the heat setting is accidentally set too high (for too long)
  if (indoorTemp > hc_TargetSetpoint)
    newHeatPercentage = _min(HEAT_DEFAULT_DAY,newHeatPercentage);
  // avoid underheating when the heat setting is accidentally set too low (for too long)
  if (indoorTemp < TEMP_SETPOINT_MIN) // 10°C
    newHeatPercentage = _max(HEAT_DEFAULT_NIGHT,newHeatPercentage);
    
  if (newHeatPercentage != curHeatPercentage)
    hc_SetHeatPercentage(newHeatPercentage,false);
  
  // basic for now
  run7021(); // update temp/rh/outdoor data
  
} // hc_loop

void hc_SetThermostatMode(uint8_t thermostatMode)
{
  hc_ThermostatMode = thermostatMode;
  switch (thermostatMode) {
    case MODE_DAY :
      hc_SetHeatPercentage (hc_HeatPercentageDay,false);
      break;
    case MODE_NIGHT :
      hc_SetHeatPercentage (hc_HeatPercentageNight,false);
      break;
    case MODE_CLOCK :
      // the heat setting will be updated by the loop function according to the heating schedule
      break;
    case MODE_FROST :
      // TODO! implement real thermostat function
      hc_SetHeatPercentage (HEAT_DEFAULT_FROST); // use the frost protection function of the gas heater
      break;
  }
  awsiot_publishUpdate();

} // hc_SetThermostatMode

uint8_t hc_GetThermostatMode(void) 
{
  return hc_ThermostatMode;
} // hc_GetThermostatMode

// heatPercentage : 0..100, manual : updates the default day/night heatPercentage setting (basic learning)
void hc_SetHeatPercentage (uint8_t heatPercentage, bool manual)
{
  if (heatPercentage > HEAT_RANGE) heatPercentage = HEAT_RANGE;
  uint32_t sdTarget = SD_TARGET_MIN + (SD_TARGET_MAX - SD_TARGET_MIN)*heatPercentage / HEAT_RANGE;
  sigmaDeltaWrite (0,(uint8_t)sdTarget);
  if (manual) {
    // do the learning
    // for now : keep it simple : take the requested heatPercentage setting for the new default
    if ((hc_ThermostatMode == MODE_DAY) || (hc_ThermostatMode == MODE_CLOCK)) hc_HeatPercentageDay = heatPercentage;
    else if (hc_ThermostatMode == MODE_NIGHT) hc_HeatPercentageNight = heatPercentage;
  }
  awsiot_publishUpdate();
  
} // hc_SetHeatPercentage

uint8_t hc_GetHeatPercentage ()
{
  uint32_t sdTarget = (uint32_t) sigmaDeltaRead ();
  uint8_t heatPercentage = (uint8_t)((sdTarget-SD_TARGET_MIN)*HEAT_RANGE/(SD_TARGET_MAX-SD_TARGET_MIN));
  return (heatPercentage);
  
} // hc_GetHeatPercentage

// web interface
void handleNestRequest()
{
  uint32_t heatPercentage = HEAT_DEFAULT_DAY;
  float targetSetpoint = TEMP_SETPOINT_DEFAULT_DAY;
  int volume = 5;
  int i;
  String message;
  uint32_t dt = ntp_GetUnixDateTime(); // browser does local time correction

  // http://mdnsname.local/nest
  if (server.args() == 0) {
    // go straight to the end
  }
  
  // http://mdnsname.local/nest?heat=85
  // http://mdnsname.local/nest?temp=23.5
  else {
    for (i=0;i<server.args();i++)
    {
      if (server.argName(i) == "heat")
      {
        heatPercentage = server.arg(i).toInt();
        if (heatPercentage > HEAT_RANGE) {
          //message = "heat setting out of range [0..100%]";
        }
        else {
          hc_SetHeatPercentage(heatPercentage);
          //message = "setting heatPercentage to " + String(heatPercentage) + "%";
        }
      }
      else if (server.argName(i) == "temp")
      {
        targetSetpoint = server.arg(i).toFloat();
        if ((targetSetpoint >= TEMP_SETPOINT_MIN) && (targetSetpoint <= TEMP_SETPOINT_MAX)) {
          hc_SetTargetSetpoint(targetSetpoint);
          //message = "setting targetSetpoint to " + String(targetSetpoint) + "oC";
        }
        else {
          //message = "temperature setting out of range [" + String(TEMP_SETPOINT_MIN) + ".." + String(TEMP_SETPOINT_MAX) + "degC]";
        }
      }
    }
  }
  message = "{\"time\": " + String(dt) + 
              ",\"targetSetpoint\":" + String(hc_TargetSetpoint) +
              ",\"heatPercentage\":" + String(hc_GetHeatPercentage()) +
              ",\"indoorTemperature\":" + String(indoorTemp) +
              ",\"indoorHumidity\":" + String(indoorHum) +
              ",\"thermostatMode\":" + String(hc_ThermostatMode) +
              ",\"freeHeap\":" + String(ESP.getFreeHeap()) +
              ",\"wifiSSID\": \"" + WiFi.SSID() +
              "\",\"wifiRSSI\":" + String(WiFi.RSSI()) + "}";             
  
  Serial.println(message);
  server.send(200, "text/plain", message);
    
} // handleNestRequest

/**************************************************************************************************************************/
/*   LOCAL FUNCTIONS 
/**************************************************************************************************************************/

static void run7021()
{
  uint32_t currentMillis = millis();
  
  if (currentMillis - lastSensorMillis > SENSOR_READ_INTERVAL) {  // Every minute, request the temperature/humidity
    float rh7021 = 0.0,temp7021 = 0.0;
    readSi7021( &rh7021,&temp7021);
    lastSensorMillis = currentMillis;
    if (temp7021 < 100.0) {
      indoorTemp = temp7021;
    }
    if (rh7021 > 0.0){ // 0.0 used as 'error reading'; could be better ..
      indoorHum = rh7021;
    }

    // update window for the moving average (used by the UI)
    indoorTemps[indoorTempsIdx++] = indoorTemp;
    if (indoorTempsIdx == NBR_INDOORTEMP_POINTS) indoorTempsIdx = 0;

    // only log to file when we have a valid NTP time
    uint32_t actualTime = ntp_GetUnixDateTime();
    if (actualTime) 
    {
        File tempLog = SPIFFS.open("/temp.csv", "a"); // Write the time and the temperature to the csv file
        tempLog.print(actualTime);
        tempLog.print(',');
        if (temp7021 < 100.0) {
          tempLog.print(temp7021);
        }
        tempLog.print(',');
        if (rh7021 > 0.0){ // 0.0 wordt gebruikt als 'error reading'; kan beter ..
          tempLog.print(rh7021);
        }
        tempLog.print(',');
        // log outdoor weather data from wunderground.com
        /*
        if (currentMillis - lastOutdoorDataMillis > OUTDOOR_LOG_INTERVAL) 
        {
          lastOutdoorDataMillis = currentMillis;
          double newOutdoorTemp = atof(wunderground.getCurrentTemp().c_str());
          // todo : nog een sanity check op de waarde die van wunderground komt
          outdoorTemp = (float) newOutdoorTemp;
          tempLog.print(newOutdoorTemp); 
        }
        */
        tempLog.print(',');
        tempLog.print(hc_GetHeatPercentage()); // log the heatPercentage to monitor the algorithm
        tempLog.println();
        tempLog.close();
    }
  }
} // run7021

// synchronous version for now --> includes 2x 150ms delay for the sensor data to be ready
// acceptable for now, reading only 1/minute
// TODO : make asynchronous!
// for reference : TemperatureLoggerInOut.ino : asynchronous reading of DS12B20 sensor
// intf : requestTemperature & requestHumidity en then wait for a fixed delay before reading data
static void readSi7021(float *rh, float *temp)
{
  unsigned int data[2] = {0,0};
 
  Wire.beginTransmission(si7021Addr);
  //Send humidity measurement command
  Wire.write(0xF5);
  Wire.endTransmission();
  delay(150);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
  // Read 2 bytes of data to get humidity
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
    // Convert the data
    float humidity  = ((data[0] * 256.0) + data[1]);
    humidity = ((125 * humidity) / 65536.0) - 6;
    *rh = humidity;
  }
  else *rh = 0.0; // reading error (in case the sensor returns odd values - due to i2c comms?
 
  Wire.beginTransmission(si7021Addr);
  // Send temperature measurement command
  Wire.write(0xF3);
  Wire.endTransmission();
  delay(150);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
 
  // Read 2 bytes of data for temperature
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
    // Convert the data
    float celsTemp  = ((data[0] * 256.0) + data[1]);
    celsTemp = ((175.72 * celsTemp) / 65536.0) - 46.85;
    *temp = celsTemp;
  }
  else {
    *temp = 100.0; // error
  }
} // readSi7021




