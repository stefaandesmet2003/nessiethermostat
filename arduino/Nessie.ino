#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h> // handelt de /update url af

#include <FS.h>

ESP8266WebServer server(80);            // create a web server on port 80
ESP8266HTTPUpdateServer httpUpdater;
File fsUploadFile;                      // a File variable to temporarily store the received file
ESP8266WiFiMulti wifiMulti;             // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'

const char OTAName[] = "NessieOTA";     // A name and a password for the OTA service
const char OTAPassword[] = "ota-password";
const char mdnsName[] = "Nessie";       // Domain name for the mDNS responder & used as hostname; 
                                        // make this different from OTAName otherwise http://mdnsName/ doesn't work
                                         
const char* www_username = "nessie";    // username & password for basic authentication to the device's server
const char* www_password = "admin";

// add Wi-Fi networks you want to connect to
const char ssid1[] = "your-ssid-1";
const char wifipwd1[] = "your-key-1";
const char ssid2[] = "your-ssid-2";
const char wifipwd2[] = "your-key-2";

// AWS IOT config --> fill in your endpoint name and thing name!
// put here for convenience -> used in awsiot.cpp
const char *aws_endpoint = "aaaaaaaaaaaaaa.iot.eu-west-1.amazonaws.com"; //AWS IOT MQTT broker
const char *aws_topic_update = "$aws/things/yourthingname/shadow/update";
const char *aws_topic_update_accepted = "$aws/things/yourthingname/shadow/update/accepted";

/**************************************************************/
/*   NESSIE APP STUFF */
/**************************************************************/
#include "myntp.h"
#include "keypad.h"
#include "heatcontrol.h"
#include "ui.h" // display stuff
#include "awsiot.h"

extern void handleNTPRequest();
extern void handleNestRequest();

/*****************************************************************************************************************************
  WIFI STUFF 
******************************************************************************************************************************/
void startWiFi() {
  if (WiFi.getMode() != WIFI_STA)
  {
      WiFi.mode(WIFI_STA); // we want this permanently in flash
  }
  WiFi.hostname(mdnsName);// MyNest --> http://mdnsName/ (als de router mee wil) met mdns ook : http://mdnsName.local/ (na startMDNS)
  WiFi.persistent(false);
  // aangezien de WiFi.persistent lijn later is toegevoegd staan de credentials hieronder al in de flash config
  // geen probleem; zelfs met lege flash config zorgt code hieronder voor een connectie

  wifiMulti.addAP(ssid1, wifipwd1);   // add Wi-Fi networks you want to connect to
  wifiMulti.addAP(ssid2, wifipwd2);
  //wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3");

  Serial.println("Connecting");
  while (wifiMulti.run() != WL_CONNECTED) {  // Wait for the Wi-Fi to connect
      delay(250);
      Serial.print('.');
  }
  Serial.println("\r\n");
  Serial.print("Connected to ");
  Serial.println(WiFi.SSID());             // Tell us what network we're connected to
  Serial.print("IP address:\t");
  Serial.print(WiFi.localIP());            // Send the IP address of the ESP8266 to the computer
  Serial.println("\r\n");  
      
  // WiFiManager alternative from weatherStationDemo, to consider!
  // configModeCallback links with UI
  /*
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  // Uncomment for testing wifi manager
  //wifiManager.resetSettings();
  wifiManager.setAPCallback(configModeCallback);

  //or use this for auto generated name ESP + ChipID
  wifiManager.autoConnect();
  */  

} // startWiFi

void startOTA() { // Start the OTA service
  ArduinoOTA.setHostname(OTAName);
  ArduinoOTA.setPassword(OTAPassword);
  
  
  ArduinoOTA.onProgress(ui_drawOtaProgress);
  // no need to route other OTA callbacks to UI
  
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\r\nEnd");
  });
  
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
} // startOTA

// Start the SPIFFS and list all contents
void startSPIFFS() { 
  SPIFFS.begin();                             // Start the SPI Flash File System (SPIFFS)
  Serial.println("SPIFFS started. Contents:");
  {
    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {                      // List the file system contents
      String fileName = dir.fileName();
      size_t fileSize = dir.fileSize();
      Serial.printf("\tFS File: %s, size: %s\r\n", fileName.c_str(), formatBytes(fileSize).c_str());
    }
    Serial.printf("\n");
  }
} // startSPIFFS

// Start the mDNS responder
void startMDNS() { 
  MDNS.begin(mdnsName);                        // start the multicast domain name server

  //stond hier niet, maar wel bij de webupdater example. Wat doet dit ????
  MDNS.addService("http", "tcp", 80);
  
  Serial.print("mDNS responder started: http://");
  Serial.print(mdnsName);
  Serial.println(".local");
} // startMDNS

// Start a HTTP server with a couple of services, and basic authentication protection
void startServer() { 

  httpUpdater.setup(&server);
  
  //list directory
  server.on("/list", HTTP_GET, handleFileList);
  //load editor
  server.on("/edit", HTTP_GET, [](){
  if(!server.authenticate(www_username, www_password))
    return server.requestAuthentication();  
  // authentication OK --> continue
    
    if(!handleFileRead("/edit.html")) server.send(404, "text/plain", "FileNotFound");
  });
  //create file
  server.on("/edit", HTTP_PUT, handleFileCreate);
  //delete file
  server.on("/edit", HTTP_DELETE, handleFileDelete);
  //first callback is called after the request has ended with all parsed arguments
  //second callback handles file uploads at that location
  server.on("/edit", HTTP_POST, [](){ server.send(200, "text/plain", ""); }, handleFileUpload);

  //get heap status, analog input value and all GPIO statuses in one json call
  // gebruikt in de index.html (zie FSBrowser example)
  server.on("/all", HTTP_GET, [](){
    String json = "{";
    json += "\"heap\":"+String(ESP.getFreeHeap());
    json += ", \"analog\":"+String(analogRead(A0));
    json += ", \"gpio\":"+String((uint32_t)(((GPI | GPO) & 0xFFFF) | ((GP16I & 0x01) << 16)));
    json += "}";
    server.send(200, "text/json", json);
    json = String();
  });
  
  server.on("/nest", HTTP_GET, handleNestRequest);                       
  server.on("/ntp", HTTP_GET, handleNTPRequest);  
  
  //called when the url is not defined here
  //use it to load content from SPIFFS
  server.onNotFound(handleNotFound);          // if someone requests any other file or page, go to function 'handleNotFound'
  // and check if the file exists

  server.begin();                             // start the HTTP server
  Serial.println("HTTP server started.");
} // startServer


/*__________________________________________________________SERVER_HANDLERS__________________________________________________________*/

void handleNotFound() { // if the requested file or page doesn't exist, return a 404 not found error
  if (!handleFileRead(server.uri())) {        // check if the file exists in the flash memory (SPIFFS), if so, send it
    server.send(404, "text/plain", "404: File Not Found");
  }
} // handleNotFound

bool handleFileRead(String path){
  if ((0 != strcmp(path.c_str(),"/privacy.html")) && (!server.authenticate(www_username, www_password))) {
    server.requestAuthentication(); 
    return false;
  }
  // authentication OK --> continue
  
  Serial.println("handleFileRead: " + path);
  if(path.endsWith("/")) path += "index.html";
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if(SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)){
    if(SPIFFS.exists(pathWithGz))
      path += ".gz";
    File file = SPIFFS.open(path, "r");
    size_t sent = server.streamFile(file, contentType);
    file.close();
    return true;
  }
  return false;
} // handleFileRead

void handleFileUpload(){
  if(!server.authenticate(www_username, www_password))
    return server.requestAuthentication();  
  // authentication OK --> continue
  
  if(server.uri() != "/edit") return;
  HTTPUpload& upload = server.upload();
  if(upload.status == UPLOAD_FILE_START){
    String filename = upload.filename;
    if(!filename.startsWith("/")) filename = "/"+filename;
    Serial.print("handleFileUpload Name: "); Serial.println(filename);
    fsUploadFile = SPIFFS.open(filename, "w");
    filename = String();
  } else if(upload.status == UPLOAD_FILE_WRITE){
    //Serial.print("handleFileUpload Data: "); Serial.println(upload.currentSize);
    if(fsUploadFile)
      fsUploadFile.write(upload.buf, upload.currentSize);
  } else if(upload.status == UPLOAD_FILE_END){
    if(fsUploadFile)
      fsUploadFile.close();
    Serial.print("handleFileUpload Size: "); Serial.println(upload.totalSize);
  }
} // handleFileUpload

void handleFileDelete(){
  if(!server.authenticate(www_username, www_password))
    return server.requestAuthentication();  
  // authentication OK --> continue
  
  if(server.args() == 0) return server.send(500, "text/plain", "BAD ARGS");
  String path = server.arg(0);
  Serial.println("handleFileDelete: " + path);
  if(path == "/")
    return server.send(500, "text/plain", "BAD PATH");
  if(!SPIFFS.exists(path))
    return server.send(404, "text/plain", "FileNotFound");
  SPIFFS.remove(path);
  server.send(200, "text/plain", "");
  path = String();
} // handleFileDelete

void handleFileCreate(){
  if(!server.authenticate(www_username, www_password))
    return server.requestAuthentication();  
  // authentication OK --> continue
  
  if(server.args() == 0)
    return server.send(500, "text/plain", "BAD ARGS");
  String path = server.arg(0);
  Serial.println("handleFileCreate: " + path);
  if(path == "/")
    return server.send(500, "text/plain", "BAD PATH");
  if(SPIFFS.exists(path))
    return server.send(500, "text/plain", "FILE EXISTS");
  File file = SPIFFS.open(path, "w");
  if(file)
    file.close();
  else
    return server.send(500, "text/plain", "CREATE FAILED");
  server.send(200, "text/plain", "");
  path = String();
} // handleFileCreate

// this is linked to the ace.js code in data/edit.html
void handleFileList() {
  if(!server.authenticate(www_username, www_password))
    return server.requestAuthentication();  
  // authentication OK --> continue
  if(!server.hasArg("dir")) {server.send(500, "text/plain", "BAD ARGS"); return;}
  
  String path = server.arg("dir");
  Serial.println("handleFileList: " + path);
  Dir dir = SPIFFS.openDir(path);
  path = String();

  String output = "[";
  while(dir.next()){
    File entry = dir.openFile("r");
    if (output != "[") output += ',';
    bool isDir = false;
    output += "{\"type\":\"";
    output += (isDir)?"dir":"file";
    output += "\",\"name\":\"";
    output += String(entry.name()).substring(1);
    output += "\"}";
    entry.close();
  }
  output += "]";
  server.send(200, "text/json", output);
} // handleFileList


/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/

String formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
  if (bytes < 1024) {
    return String(bytes) + "B";
  } else if (bytes < (1024 * 1024)) {
    return String(bytes / 1024.0) + "KB";
  } else if (bytes < (1024 * 1024 * 1024)) {
    return String(bytes / 1024.0 / 1024.0) + "MB";
  }
} // formatBytes

String getContentType(String filename){ // determine the filetype of a given filename, based on the extension
  if(server.hasArg("download")) return "application/octet-stream";
  else if(filename.endsWith(".htm")) return "text/html";
  else if(filename.endsWith(".html")) return "text/html";
  else if(filename.endsWith(".css")) return "text/css";
  else if(filename.endsWith(".js")) return "application/javascript";
  else if(filename.endsWith(".png")) return "image/png";
  else if(filename.endsWith(".gif")) return "image/gif";
  else if(filename.endsWith(".jpg")) return "image/jpeg";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".xml")) return "text/xml";
  else if(filename.endsWith(".pdf")) return "application/x-pdf";
  else if(filename.endsWith(".zip")) return "application/x-zip";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
} // getContentType

void setup() 
{
    pinMode(LED_BUILTIN, OUTPUT);
    
    Serial.begin(115200);        // Start the Serial communication to send messages to the computer
    hc_setup();
    keypad_setup();
    
    startWiFi();                 // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
    startOTA();                  // Start the OTA service
    startSPIFFS();               // Start the SPIFFS and list all contents
    startMDNS();                 // Start the mDNS responder
    startServer();               // Start a HTTP server with a file read handler and an upload handler
    startNTP();                  // Start listening for UDP messages to port 123
    
    awsiot_setup();               // start the mqtt connection to AWS IOT
    ui_setup();    

} // setup

void loop() 
{
    server.handleClient();  // run the server
    ArduinoOTA.handle();    // listen for OTA events
    runNTP();               // periodically update the time
    
    keypad_loop();          // scan touch keys
    ui_loop();              // display user interface
    hc_loop();              // heating control
    awsiot_loop();           // serve the mqtt connection
 
} // loop

