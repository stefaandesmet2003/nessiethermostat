#ifndef _MYNTP_H_
#define _MYNTP_H_

void startNTP();
void runNTP();

#define NTP_UTC_OFFSET 1 // +1 uur tov GMT
// return : 0 if no ntp time obtained yet, else utc offset corrected unix time
uint32_t ntp_GetDateTime();
uint32_t ntp_GetUnixDateTime();

#endif // _MYNTP_H_
