var AWS = require('aws-sdk');
AWS.config.region = "eu-west-1";
// add your endpoint here!
var iotData = new AWS.IotData({endpoint: "aaaaaaaaaaaaaa.iot.eu-west-1.amazonaws.com"});
const thingName = "your-AWS-IOT-thingname";

var targetSetpoint = 23.5;
var indoorTemperature = 21.9;
var thermostatMode = "HEAT";
var heatPercentage = 75; // integer 0..100 according alexa doc
var endpointHealthOK = true;
const MAX_TARGETSETPOINT = 28.0;
const MIN_TARGETSETPOINT = 10.0;

var lastTimeOfSample = "2018-02-06T10:20:50.52Z"; // a starting point

function log(message, message1, message2) {
    console.log(message + message1 + message2);
} // log

function getEndpointHealth() {
  // todo : check connection to thing
  return "OK";
} // getEndpointHealth

function getUncertaintyInMilliseconds() {
  // todo !
  return 0;
}

// temp : callback = NULL -> return cached properties
function getEndpointProperties(callback) {
  // start off with endpointProperties as currently cashed in this module
  var timeOfSample = lastTimeOfSample;
  var uncertaintyInMilliseconds = getUncertaintyInMilliseconds();
  var endpointProperties = [
    {
        "namespace": "Alexa.ThermostatController",
        "name": "targetSetpoint",
        "value": {
          "value" : targetSetpoint,
          "scale" : "CELSIUS"
        } ,
        "timeOfSample": timeOfSample, 
        "uncertaintyInMilliseconds": uncertaintyInMilliseconds
    },
    {
        "namespace": "Alexa.ThermostatController",
        "name": "thermostatMode",
        "value": thermostatMode ,
        "timeOfSample": timeOfSample,
        "uncertaintyInMilliseconds": uncertaintyInMilliseconds
    },
    {
        "namespace": "Alexa.TemperatureSensor",
        "name": "temperature",
        "value": {
          "value" : indoorTemperature,
          "scale" : "CELSIUS"
        } ,
        "timeOfSample": timeOfSample,
        "uncertaintyInMilliseconds": uncertaintyInMilliseconds
    },
    {
        "namespace": "Alexa.PercentageController",
        "name": "percentage",
        "value": heatPercentage,
        "timeOfSample": timeOfSample,
        "uncertaintyInMilliseconds": uncertaintyInMilliseconds
    },    
    {
        "namespace": "Alexa.EndpointHealth",
        "name": "connectivity",
        "value": {
            "value": getEndpointHealth()
        },
        "timeOfSample": timeOfSample,
        "uncertaintyInMilliseconds": uncertaintyInMilliseconds
    }        
  ];
  if (callback) {
    var params = {thingName : thingName};
    iotData.getThingShadow(params, function(err,data) {
      if (err) {
        console.log(err, err.stack);
        // will reply cached endpoint properties
      }
      else {
        console.log(data);
        var jsonData = JSON.parse(data.payload);
        
        if (jsonData.state.reported) {
          // update cached values
          heatPercentage = jsonData.state.reported.heatPercentage;
          thermostatMode = jsonData.state.reported.thermostatMode;
          targetSetpoint = jsonData.state.reported.targetSetpoint;
          indoorTemperature = jsonData.state.reported.indoorTemperature;
          timeOfSample = new Date(jsonData.timestamp*1000).toISOString();
          lastTimeOfSample = timeOfSample; // update the cached value
          // todo : update endpointProperties here, but alexa will request updates anyway
          endpointProperties[0].value.value = targetSetpoint;
          endpointProperties[1].value = thermostatMode;
          endpointProperties[2].value.value = indoorTemperature;
          endpointProperties[3].value = heatPercentage;
          // update will happen on next call to getEndpointProperties
          
          var i;
          // no need now to copy the individual property's timestamps
          for (i=0;i<endpointProperties.length;i++) 
          {
            endpointProperties[i].timeOfSample = timeOfSample;
          }
        }
      }
      callback(endpointProperties);
    });
  }
  else {
    return (endpointProperties); // return the cached version synchronously
  }
  // console.log("getEndpointProperties sync part finished! waiting for callback from aws iot!");
} // getEndpointProperties

function handleDiscoveryRequest(request, context) {

    // get user token passed in request
    var requestToken = request.directive.payload.scope.token;
    // for the discovery request the token is in the payload!!
    // TODO : handle the token

    var responsePayload = {
        "endpoints":
        [
          {
            "endpointId": "sdsEndpointId",
            "manufacturerName": "Stefaan's Smart Device Company",
            "friendlyName": "Living Room Thermostat",
            "description": "Junkers TW2 compatible smart thermostat",
            "displayCategories":["THERMOSTAT", "TEMPERATURE_SENSOR" ],
            "cookie": {
            },
            "capabilities": [
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa",
                    "version": "3"
                },
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa.ThermostatController",
                    "version": "3",
                    "properties": {
                        "supported": [
                            {
                                "name": "targetSetpoint"
                            },
                            {
                                "name": "thermostatMode"
                            }
                        ],
                        "proactivelyReported": false,
                        "retrievable": true
                    }
                },
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa.TemperatureSensor",
                    "version": "3",
                    "properties": {
                        "supported": [
                            {
                                "name": "temperature"
                            }
                        ],
                        "proactivelyReported": false,
                        "retrievable": true
                    }
                },
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa.PercentageController",
                    "version": "3",
                    "properties": {
                        "supported": [
                            {
                                "name": "percentage"
                            }
                        ],
                        "proactivelyReported": false,
                        "retrievable": true
                    }
                },
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa.EndpointHealth",
                    "version": "3",
                    "properties": {
                        "supported": [
                            {
                                "name": "connectivity"
                            }
                        ],
                        "proactivelyReported": false,
                        "retrievable": true
                    }
                }
            ]
          },
        ]
    };
    
    var responseHeader = request.directive.header;
    responseHeader.name = "Discover.Response";
    //responseHeader.messageId = responseHeader.messageId + "-R";
    var response = {
        event: {
            header: responseHeader,
            payload : responsePayload
        }
    };
    log("DEBUG:", "Discover.Response ", JSON.stringify(response));
    context.succeed(response);
    
} // handleDiscoveryRequest

function handleReportStateRequest(request, context) {
    // get user token passed in request
    var requestToken = request.directive.endpoint.scope.token; // todo : handle the token
    // the endpointProperties will arrive only on the callback from iotData.getThingShadow
    getEndpointProperties(function callback(endpointProperties) {
      var contextResult = {
          "properties": endpointProperties
      };
      var responseHeader = request.directive.header;
      responseHeader.name = "StateReport";
      responseHeader.messageId = responseHeader.messageId + "-R";
      var response = {
          context: contextResult,
          event: {
              header: responseHeader,
              endpoint : request.directive.endpoint,
              payload: {}
          },
      };
      log("DEBUG:", "StateReport ", JSON.stringify(response));
      context.succeed(response);
    });

} // handleReportStateRequest

function buildGenericAlexaErrorResponse(request,errorType, errorMessage) {
  var errorResponseHeader = request.directive.header;
  
  // errors for this controller are in the Alexa namespace
  errorResponseHeader.namespace = "Alexa";
  errorResponseHeader.name = "ErrorResponse";
  errorResponseHeader.messageId = errorResponseHeader.messageId + "-R";
  // leave the other fields untouched
  
  var errorResponsePayload = {
    type: errorType,
    message : errorMessage
  };
  if (errorType === "VALUE_OUT_OF_RANGE") {
    // add a valid range object to the payload
    errorResponsePayload.validRange = {
      minimumValue : 0,
      maximumValue : 100
    };
  }

  var errorResponse = {
      event: {
          header: errorResponseHeader,
          endpoint : request.directive.endpoint,
          payload: errorResponsePayload
      }
  };
  return (errorResponse);
  
} // buildGenericAlexaErrorResponse

function ThermostatControllerErrorResponse(request,errorType, errorMessage) {
  var errorResponseHeader = request.directive.header;
  
  if (errorType === "TEMPERATURE_VALUE_OUT_OF_RANGE") {
    // for one odd reason this error is in the Alexa namespace
    errorResponseHeader.namespace = "Alexa";
    // test
    errorType = "INVALID_VALUE";
  }
  else 
    errorResponseHeader.namespace = "Alexa.ThermostatController";
  errorResponseHeader.name = "ErrorResponse";
  errorResponseHeader.messageId = errorResponseHeader.messageId + "-R";
  // leave the other fields untouched
  
  var errorResponsePayload = {
    type: errorType,
    message : errorMessage
  };
  if (errorType === "TEMPERATURE_VALUE_OUT_OF_RANGE") {
    // add a valid range object to the payload

    errorResponsePayload.validRange = {
      minimumValue : { 
        value : MIN_TARGETSETPOINT,
        scale : "CELSIUS"
      },
      maximumValue : {
        value : MAX_TARGETSETPOINT,
        scale : "CELSIUS"
      }
    };
   
    errorResponsePayload.validRange = {
      minimumValue : MIN_TARGETSETPOINT, 
      maximumValue : MAX_TARGETSETPOINT
    };
  }
  if (errorType === "VALUE_OUT_OF_RANGE") {
    // add a valid range object to the payload
    errorResponsePayload.validRange = {
      minimumValue : MIN_TARGETSETPOINT, 
      maximumValue : MAX_TARGETSETPOINT
    };
  }

  var errorResponse = {
      event: {
          header: errorResponseHeader,
          endpoint : request.directive.endpoint,
          payload: errorResponsePayload
      }
  };
  return (errorResponse);
  
} // ThermostatControllerErrorResponse

function handleThermostatControllerRequest(request, context) {
    // get device ID passed in during discovery
    var requestMethod = request.directive.header.name; // SetTargetTemperature
    // get user token pass in request
    var requestToken = request.directive.endpoint.scope.token; // todo : handle the token
    // the payload contains  the targetSetpoint or targetSetpointDelta
    var newTargetSetpoint = -300.0;
    var response;
    var iotPayload;
    var params;
    
    if (requestMethod === "SetThermostatMode") {
      // we don't support "COOL"
      if (request.directive.payload.thermostatMode) {
        if (request.directive.payload.thermostatMode.value != "COOL") {
          thermostatMode = request.directive.payload.thermostatMode.value;
          // todo : OFF and ECO to affect the targetSetpoint
          newTargetSetpoint = targetSetpoint;
          // update thing shadow 
          iotPayload = {
            state : {
              desired : {
              thermostatMode : thermostatMode,
              targetSetpoint : targetSetpoint
              }
            }
          };
          params = {
            thingName : thingName,
            payload : JSON.stringify(iotPayload)
          };
          iotData.updateThingShadow(params, function(err,data) {
            if (err) {
              console.log(err, err.stack);
              context.succeed(buildGenericAlexaErrorResponse(request,"INTERNAL_ERROR","Error response from AWS IOT")); // return "INTERNAL_ERROR"
            }
            else {
              console.log(data);
              var jsonData = JSON.parse(data.payload);
              // todo : check response data
              lastTimeOfSample = new Date(jsonData.timestamp*1000).toISOString(); // update the cached value
              // todo : we could request a shadow update here, and respond to alexa with the updated properties
              // but alexa is requesting state reports anyway, so we will update there only
              var contextResult = {
                  "properties": getEndpointProperties(0) // we returnen voorlopig de cached properties
              };
              var responseHeader = request.directive.header;
              responseHeader.namespace = "Alexa";
              responseHeader.name = "Response";
              responseHeader.messageId = responseHeader.messageId + "-R";
              var response = {
                  context: contextResult,
                  event: {
                      header: responseHeader,
                      endpoint : request.directive.endpoint,
                      payload: {}
                  },
              };
              log("DEBUG:", "Alexa.ThermostatController ", JSON.stringify(response));
              context.succeed(response);
            }
          }); //end callback part
        }
        else {
          response = ThermostatControllerErrorResponse(request,"UNSUPPORTED_THERMOSTAT_MODE","Unsupported feature");
          log("DEBUG:", "Alexa.ThermostatController ", JSON.stringify(response));
          context.succeed(response);
          return;
        }
      }
    }
    else {
      if (requestMethod === "SetTargetTemperature" ) {
        if ((request.directive.payload.lowerSetpoint) || (request.directive.payload.upperSetpoint)) {
          // not supported error
          if (request.directive.payload.targetSetpoint) {
            response = ThermostatControllerErrorResponse(request,"TRIPLE_SETPOINTS_UNSUPPORTED","Unsupported feature");
          }
          else {
            response = ThermostatControllerErrorResponse(request,"DUAL_SETPOINTS_UNSUPPORTED","Unsupported feature");
          }
          log("DEBUG:", "Alexa.ThermostatController ", JSON.stringify(response));
          context.succeed(response);
          return;
        }
        
        if (request.directive.payload.targetSetpoint) {
          if (request.directive.payload.targetSetpoint.scale == "CELSIUS") {
            newTargetSetpoint = request.directive.payload.targetSetpoint.value;
          }
          else if (request.directive.payload.targetSetpoint.scale == "FAHRENHEIT") {
            // convert to celsius
            newTargetSetpoint = (newTargetSetpoint = request.directive.payload.targetSetpoint.value - 32)*5/9;
          }
          else if (request.directive.payload.targetSetpoint.scale == "KELVIN") {
            // convert to celsius
            newTargetSetpoint = newTargetSetpoint = request.directive.payload.targetSetpoint.value - 273.15;
          }
        }
      }
      else if (requestMethod === "AdjustTargetTemperature") {
        if (request.directive.payload.targetSetpointDelta) {
          if ((request.directive.payload.targetSetpointDelta.scale == "CELSIUS") || 
              (request.directive.payload.targetSetpointDelta.scale == "KELVIN"))
          {
            newTargetSetpoint = targetSetpoint + request.directive.payload.targetSetpointDelta.value;
          }
          else if (request.directive.payload.targetSetpointDelta.scale == "FAHRENHEIT") {
            // convert to celsius
            newTargetSetpoint = targetSetpoint + (request.directive.payload.targetSetpointDelta.value - 32)*5/9;
          }
        }
      }
      // sanity check
      if ((newTargetSetpoint >= MIN_TARGETSETPOINT) && (newTargetSetpoint <=MAX_TARGETSETPOINT)) {
        targetSetpoint = newTargetSetpoint;
        // send the request to the aws iot thingshadow!
        iotPayload = {
          state : {
            desired : {
              thermostatMode : thermostatMode,
              targetSetpoint : targetSetpoint
            }
          }
        };
        params = {
          thingName : thingName,
          payload : JSON.stringify(iotPayload)
        };
        iotData.updateThingShadow(params, function(err,data) {
          if (err) {
            console.log(err, err.stack);
            context.succeed(buildGenericAlexaErrorResponse(request,"INTERNAL_ERROR","Error response from AWS IOT")); // return "INTERNAL_ERROR"
          }
          else {
            console.log(data);
            var jsonData = JSON.parse(data.payload);
            // todo : check response data
            lastTimeOfSample = new Date(jsonData.timestamp*1000).toISOString(); // update the cached value
            // todo : we could request a shadow update here, and respond to alexa with the updated properties
            // but alexa is requesting state reports anyway, so we will update there only
            var contextResult = {
                "properties": getEndpointProperties(0) // we returnen voorlopig de cached properties
            };
            var responseHeader = request.directive.header;
            responseHeader.namespace = "Alexa";
            responseHeader.name = "Response";
            responseHeader.messageId = responseHeader.messageId + "-R";
            var response = {
                context: contextResult,
                event: {
                    header: responseHeader,
                    endpoint : request.directive.endpoint,
                    payload: {}
                },
            };
            log("DEBUG:", "Alexa.ThermostatController ", JSON.stringify(response));
            context.succeed(response);
          }
        }); //end callback part
      }
      else {
        // todo : return error response
        response = ThermostatControllerErrorResponse(request,"TEMPERATURE_VALUE_OUT_OF_RANGE","Invalid temperature requested");
        log("DEBUG:", "Alexa.ThermostatController ", JSON.stringify(response));
        context.succeed(response);
        return;
      }
      console.log("handleThermostatControllerRequest : end of sync part");
      //context.succeed(response);
    }
} // handleThermostatControllerRequest

function handlePercentageControllerRequest(request, context) {
    // get device ID passed in during discovery
    var requestMethod = request.directive.header.name; // SetTargetTemperature
    // get user token pass in request
    var requestToken = request.directive.endpoint.scope.token; // todo : handle the token
    // the payload contains  the percentage or percentageDelta
    var newHeatPercentage = heatPercentage;
    var response;
    var iotPayload;
    var params;    

    if (requestMethod === "SetPercentage" ) {
      if (request.directive.payload.percentage) {
        newHeatPercentage = request.directive.payload.percentage;
      }
    }
    else if (requestMethod === "AdjustPercentage") {
      if (request.directive.payload.percentageDelta) {
        newHeatPercentage = heatPercentage + request.directive.payload.percentageDelta;
      }
    }
    // sanity check
    if ((newHeatPercentage >= 0) && (newHeatPercentage <=100)) {
      heatPercentage = newHeatPercentage;
      
      // update AWS IOT thing shadow 
      iotPayload = {
        state : {
          desired : {
          heatPercentage : heatPercentage
          }
        }
      };
      params = {
        thingName : thingName,
        payload : JSON.stringify(iotPayload)
      };
      iotData.updateThingShadow(params, function(err,data) {
        if (err) {
          console.log(err, err.stack);
          context.succeed(buildGenericAlexaErrorResponse(request,"INTERNAL_ERROR","Error response from AWS IOT")); // return "INTERNAL_ERROR"
        }
        else {
          console.log(data);
          var jsonData = JSON.parse(data.payload);
          // todo : check response data
          lastTimeOfSample = new Date(jsonData.timestamp*1000).toISOString(); // update the cached value
          // todo : we could request a shadow update here, and respond to alexa with the updated properties
          // but alexa is requesting state reports anyway, so we will update there only
          var contextResult = {
              "properties": getEndpointProperties(0) // we returnen voorlopig de cached properties
          };
          var responseHeader = request.directive.header;
          responseHeader.namespace = "Alexa";
          responseHeader.name = "Response";
          responseHeader.messageId = responseHeader.messageId + "-R";
          var response = {
              context: contextResult,
              event: {
                  header: responseHeader,
                  endpoint : request.directive.endpoint,
                  payload: {}
              },
          };
          log("DEBUG:", "Alexa.PercentageController ", JSON.stringify(response));
          context.succeed(response);
        }
      }); //end callback part
    }
    else {
      // no idea if the alexa app will prevent out of range values, catch them here
      response = buildGenericAlexaErrorResponse(request,"VALUE_OUT_OF_RANGE","Invalid percentage  requested");
      log("DEBUG:", "Alexa.PercentageController", JSON.stringify(response));
      context.succeed(response);
      return;
    }
    
} // handlePercentageControllerRequest

exports.handler = function (request, context) {

    // Alexa.Discovery
    if ((request.directive.header.namespace === 'Alexa.Discovery') && (request.directive.header.name === 'Discover')) {
      handleDiscoveryRequest(request, context, "");
    }
    // Alexa (ReportState)
    else if ((request.directive.header.namespace === 'Alexa') && (request.directive.header.name === 'ReportState')) {
      handleReportStateRequest(request, context);
    }
    // ThermostatController interface
    else if (request.directive.header.namespace === 'Alexa.ThermostatController') {
      handleThermostatControllerRequest(request, context);
    }
    // nothing to do for TemperatureSensor interface -> uses Alexa.ReportState
    // PercentageController interface
    else if (request.directive.header.namespace === 'Alexa.PercentageController') {
      handlePercentageControllerRequest(request, context);
    }
    // nothing to do for TemperatureSensor interface -> uses Alexa.ReportState
    else {
      // let's print out these unexpected requests
      log("DEBUG:", "Unknown Request",  JSON.stringify(request));
      log("DEBUG:", "Context",  JSON.stringify(context));
    }

}; // exports.handler